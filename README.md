
## Features

Please see our [gulpfile](gulpfile.js) for up to date information on what we support.

* enable [ES2015 features](https://babeljs.io/docs/learn-es2015/) using [Babel](https://babeljs.io)
* CSS Autoprefixing
* Built-in preview server with BrowserSync
* Automagically compile Sass with [libsass](http://libsass.org)
* Map compiled CSS to source stylesheets with source maps
* Image optimization

## What plugins are connected in stock
* [Device.js](https://github.com/matthewhudson/device.js)

## Getting Started

- Install: `npm install`
- Run `gulp` to preview and watch for changes (develop version)
- Run `gulp build` to build your site for production
- Run `gulp build-no-images` to build your site without images for production
- Run `gulp build-test` to preview the production build
- Run `build-minify` to build and minify for production

## License

[BSD license](http://opensource.org/licenses/bsd-license.php)
